# Thorgate's Gatsby Starter

Gatsby Starter that we use at Thorgate.

It's configured for using Netlify CMS and store content in Markdown files in a repository.

## Quick start

Creating a new project is very simply and so is running it locally. 

```bash
# Firstly install gatsby-cli util
yarn global add gatsby-cli

# Create a new project with this starter as a template
# It clones the starter's repository with squashing commits to initial one
# and installs all required dependencies 
gatsby new awsomeproject https://gitlab.com/thorgate/thorgate-gatsby-starter

# Run the site
yarn start
```

**NOTE:** It's important to change url to your repository in
[static/admin/config.yml](static/admin/config.yml)

Note that there's also a [`.nvmrc`](./.nvmrc) file in the root directory that
provides the Node version that is intended to be used with this project.

Finally, open your browser at [http://127.0.0.1:8000](http://127.0.0.1:8000)!

## Netlify CMS

The admin interface for Netlify CMS is available on the `/admin` URL
([http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)). Whenever a user
makes a change and clicks "Publish", then the markdown file(s) will be modified
and those changes will be committed to the repository. This project has some
configuration that makes these commits onto the currently checked out branch
(the default of Netlify CMS is to just commit all changes to `master`).

The CMS is configured in [static/admin/config.yml](static/admin/config.yml) and
[src/cms/cms.js](src/cms/cms.js).

**NOTE:** Netlify CMS will modify files on your disk in development mode.

## Production set-up & deploys

You should do the next simple steps to make initial deploy the project to Netlify.
1. Create a new site and choose Gitlab as a provider.
2. Choose the project's repository.
3. Choose repository's branch.

That's it! Wait until the deployment is finished and enjoy your new site!

**NOTE:** To deploy the project to Netlify you have to have admin rights for the project's
repository. 

Further, Netlify will do rebuild each time when something was pushed to the selected branch.

## Learning Gatsby & Netlify CMS

Looking for guidance on how Gatsby works? Full documentation for Gatsby lives
[on the website](https://www.gatsbyjs.org/). Here are some places to start:

-   **For most developers, we recommend starting with our [in-depth tutorial for
    creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts
    with zero assumptions about your level of ability and walks through every step
    of the process.

-   **To dive straight into code samples, head [to our
    documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the
    _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

-   [Gatsby with Netlify CMS tutorial](https://www.gatsbyjs.org/tutorial/blog-netlify-cms-tutorial/)

-   [Sales page Gatsby & Netlify CMS (similar to the set up of this
    site)](https://www.frontendstumbles.com/gatsby-and-netlify-cms-tutorial/)

## Project structure
```bash
.
├─ src
│  ├─ cms
│  │  ├─ cms.js                     # - Initialization of CMS
│  │  └─ previews                   # - Keeps components that generate preview of pages in admin
│  │     └─ HomePagePreview.js     #   isn't required, but shows page how will it look for user
│  ├─ components                    # - Bunch of useful components from the basic gatsby starter
│  │  ├─ Header.js
│  │  ├─ index.js
│  │  ├─ Layout.css
│  │  ├─ Layout.js                  # - Default page layout, pages are being rendered inside
│  │  ├─ PreviewCompatibleImage.js
│  │  └─ SEO.js
│  ├─ pages                         # - Keeps site's pages either react component which is 
│  │  ├─ 404.js                     # - available directly (http://127.0.0.1:8000/404)
│  │  └─ index.md                   # - or as Markdown file which is required binding (see below)
│  └─ templates
│     └─ HomePageTemplate.js        # - Pages' teplates, the name have to be the same as 
├─ static                           #   templateKey in .md file
│  ├─ admin
│  │  └─ config.yml                 # - Configuration of CMS, describes .md files and binds them 
│  └─ assets                        #   to pages
│     ├─ copy-of-img_5139-min.jpg
│     └─ gatsby-icon.png
```

## Debugging FAQ

1. **Q: "I changed something in the content Markdown files and I'm getting a
   weird error"**

    Hot-reloading doesn't reload Markdown files quite so well. The first thing to
    try is to just refresh the page.

    If refreshing doesn't work, then another good idea is to delete the `.cache/`
    folder and restart the server. This is especially useful when getting some
    GraphQL error related to fetching images.

    ```bash
    # (close the running server with Ctrl+C)
    rm -rf .cache
    yarn start
    ```

    Another folder that you can try to delete is the `public/` folder but that
    seems less useful.
