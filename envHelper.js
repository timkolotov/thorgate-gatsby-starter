const gitBranch = require('git-branch');

/**
 * Set the environment variables that can be used to configure Netlify CMS in
 * `src/cms/cms.js`.
 *
 * Only environment variables with the `GATSBY_` prefix are available on the
 * client side, so we need to copy environment variables to use suitable names.
 * https://www.gatsbyjs.org/docs/environment-variables/
 */
const setEnvironmentVariables = () => {
    // Check `src/cms/cms.js` to see how this environment variable is used.
    if (process.env.NETLIFY) {
        // When deployed to Netlify, there is an environment variable `HEAD`
        // available that shows the current branch.
        process.env.GATSBY_BRANCH = process.env.HEAD;
    } else {
        // When running locally, we use the `git-branch` package to detect the
        // branch.
        process.env.GATSBY_BRANCH = gitBranch.sync();
    }

    // Keep track of whether we're running in development mode or not
    process.env.GATSBY_NODE_ENV = process.env.NODE_ENV;
};

module.exports = {
    setEnvironmentVariables,
};
