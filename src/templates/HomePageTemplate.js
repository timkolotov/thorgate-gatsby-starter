import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import {graphql} from 'gatsby';

import {Layout, PreviewCompatibleImage} from 'components';

export const HomePageTemplate = ({title, intro}) => (
    <article>
        <h2>{title}</h2>
        <section>
            <h3>{intro.title}</h3>
            <div style={{maxWidth: 400}}>
                <PreviewCompatibleImage imageInfo={intro.image} />
            </div>
            <ReactMarkdown source={intro.contents} escapeHtml={false} />
        </section>
    </article>
);

HomePageTemplate.propTypes = {
    title: PropTypes.string.isRequired,
    intro: PropTypes.shape({
        title: PropTypes.string.isRequired,
        contents: PropTypes.string.isRequired,
        image: PropTypes.object.isRequired,
    }).isRequired,
};

const HomePage = ({data}) => (
    <Layout>
        <HomePageTemplate {...data.markdownRemark.frontmatter} />
    </Layout>
);

HomePage.propTypes = {
    data: PropTypes.shape({
        markdownRemark: PropTypes.shape({
            frontmatter: PropTypes.shape(HomePageTemplate.propTypes).isRequired,
        }).isRequired,
    }).isRequired,
};

export default HomePage;

export const pageQuery = graphql`
    query HomePageTemplate($id: String) {
        markdownRemark(id: {eq: $id}) {
            frontmatter {
                title
                intro {
                    title
                    contents
                    image {
                        alt
                        image {
                            childImageSharp {
                                fluid(maxWidth: 200) {
                                    ...GatsbyImageSharpFluid
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;
