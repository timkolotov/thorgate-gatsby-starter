import React from 'react';
import renderer from 'react-test-renderer';
import {useStaticQuery} from 'gatsby';
import HomePageTemplate from './HomePageTemplate';

const homePageData = {
    markdownRemark: {
        frontmatter: {
            title: 'Page Title',
            intro: {
                title: 'Second Page Title',
                contents: 'PAge content',
                image: {
                    image: 'http://url_to_image/image.png',
                },
            },
        },
    },
};

beforeEach(() => {
    useStaticQuery.mockImplementationOnce(() => ({
        site: {
            siteMetadata: {
                title: `Default Starter`,
            },
        },
    }));
});

describe('HomePageTemplate', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<HomePageTemplate data={homePageData} />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
