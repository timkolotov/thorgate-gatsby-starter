import React from 'react';
import {render} from '@testing-library/react';
import Header from './Header';

describe('Header', () => {
    it('displays the correct title', () => {
        const title = 'Default Starter';

        const {getByTestId} = render(<Header siteTitle={title} />);
        expect(getByTestId('header-link')).toHaveTextContent(title);
    });
});
