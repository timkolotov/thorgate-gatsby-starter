import CMS, {init} from 'netlify-cms-app';

import HomePagePreview from './previews/HomePagePreview';

CMS.registerPreviewTemplate('home', HomePagePreview);

/**
 * The default behavior of Netlify CMS is to commit all changes, that are done
 * in the admin, to `master` but it makes a lot more sense to commit to the
 * branch that is currently running (deployed to Netlify), so there is a bit of
 * a hack to get this feature.
 *
 * We rely on an environment variable `GATSBY_BRANCH` to contain the
 * current Git branch name. Then we just manually configure Netlify CMS to
 * commit to that branch. The environment variable is set by the `envHelper.js`
 * script.
 *
 * It's important to note that the environment variable is populated when the
 * Gatsby server is restarted (so, if the server is running and another branch
 * is checked out, then the changes in the admin will be committed to the old
 * branch). It's required to restart the server after the branch has changed!
 *
 * Another thing we can do here is to enable running Netlify CMS locally so
 * that it does not make any commits, but modifies the files in place. This is
 * done by setting the `local_backend` setting to `true`
 * https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository
 *
 * NOTE: This depends on setting `manualInit: true` in
 * `gatsby-config.js`.
 *
 * Took the solution from this GitHub issue comment:
 * https://github.com/netlify/netlify-cms/issues/1795#issuecomment-556962870
 */
const initialize = () => {
    const branch = process.env.GATSBY_BRANCH ?? 'master';

    const config = {
        backend: {
            branch,
        },
    };

    const {GATSBY_NODE_ENV} = process.env;
    if (GATSBY_NODE_ENV === 'development') {
        config.local_backend = true;
    }

    init({config});
};

initialize();
