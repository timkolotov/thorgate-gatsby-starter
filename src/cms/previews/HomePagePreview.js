import React from 'react';
import PropTypes from 'prop-types';

import {HomePageTemplate} from 'templates/HomePageTemplate';

const HomePagePreview = ({entry, getAsset}) => {
    const data = entry.getIn(['data']).toJS();

    if (!data) {
        return <div>Loading....</div>;
    }

    return (
        <HomePageTemplate
            title={data.title}
            intro={{
                ...data.intro,
                image: {
                    image: getAsset(data.intro.image.image),
                    ...data.intro.image,
                },
            }}
        />
    );
};

HomePagePreview.propTypes = {
    entry: PropTypes.object.isRequired,
    getAsset: PropTypes.func.isRequired,
};

export default HomePagePreview;
