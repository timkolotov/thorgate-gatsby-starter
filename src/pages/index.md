---
templateKey: HomePageTemplate
title: Home Page
intro:
  title: Hello world
  contents: >-
    We are looking for people who can help us on our mission to change the world
    with technology and make it sustainable. 


    <div class="row">
      <div class="col-md-6">First column</div>
      <div class="col-md-6">Another column</div>
    </div>


    **Join us!**
  image:
    image: /assets/copy-of-img_5139-min.jpg
    alt: Volleyball
---
