const path = require('path');
const {createFilePath} = require('gatsby-source-filesystem');
const {fmImagesToRelative} = require('gatsby-remark-relative-images');

exports.createPages = async ({actions, graphql}) => {
    const {createPage} = actions;

    const {errors, data} = await graphql(`
        {
            allMarkdownRemark(limit: 1000) {
                edges {
                    node {
                        id
                        frontmatter {
                            templateKey
                        }
                        fields {
                            slug
                        }
                    }
                }
            }
        }
    `);

    if (errors) {
        errors.forEach(e => console.error(e.toString()));
        return Promise.reject(errors);
    }

    const posts = data.allMarkdownRemark.edges;

    posts.forEach(edge => {
        const id = edge.node.id;
        createPage({
            path: edge.node.fields.slug,
            component: path.resolve(
                `src/templates/${String(edge.node.frontmatter.templateKey)}.js`,
            ),
            // ID can be used in the page query for filtering
            context: {
                id,
            },
        });
    });
};

exports.onCreateNode = ({node, actions, getNode}) => {
    const {createNodeField} = actions;
    fmImagesToRelative(node); // convert image paths for gatsby images

    if (node.internal.type === 'MarkdownRemark') {
        const value = createFilePath({node, getNode});
        createNodeField({
            name: 'slug',
            node,
            value,
        });
    }
};
